# Extract PNG from R&S Set

This repository contains a script that extracts PNG screenshots from Rohde&Schwarz .set files.

Tested on a bunch of files from an FSH4 Spectrum Analyser.

## Usage

Run

```bash
extract-png-from-rs-set [files]
```

where `[files]` either is a filename, a list of filenames, or (tested with Bash only)
a filter string like `measurement*.set`.

## Disclaimer

This script has nothing to do with the company Rohde&Schwarz. Please don't bother them when there's
a problem with this script.

Feedback via my Gitlab repository (https://gitlab.com/cweickhmann/extract-png-from-rs-set),
i.e. bother me ;-) .

This software is provided as is. Using it may cause data loss.
Use with care. It is advised to work on copies of the original files.

## License

MIT License

Copyright (c) 2022 Christian Weickhmann

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
